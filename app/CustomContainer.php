<?php

namespace DI;

use Nette\DI\Container;
use Nette\InvalidArgumentException;

class CustomContainer extends Container
{
    /**
     * Calls all methods starting with with "inject" using autowiring.
     *
     * @param $service
     * @throws \Nette\InvalidArgumentException
     * @return void
     */
    public function callInjects($service)
    {
        if (!is_object($service))
        {
            throw new InvalidArgumentException("Service must be object, " . gettype($service) . " given.");
        }

        foreach (array_reverse(get_class_methods($service)) as $method)
        {
            if (substr($method, 0, 6) === 'inject')
            {
                $this->callMethod(array($service, $method));
            }
        }
    }
}
