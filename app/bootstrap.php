<?php
require __DIR__ . '/../libs/autoload.php';

$configurator = new Nette\Configurator;

// **********************
// **** ENVIRONMENT *****
// **********************

// zisti enviroment zo suboru - default je production
$environmentFile = __DIR__ . '/config/environment';
$environment = (file_exists($environmentFile))
    ? file_get_contents($environmentFile)
    : 'production';

// **********************
// **** DEBUGGING *******
// **********************

// Zapnute debugovanie - mimo produkcie vzdy
$debugMode = ($environment === 'production') ? FALSE : TRUE;
$configurator->setDebugMode($debugMode);
$configurator->enableDebugger(__DIR__ . '/../log');

// **********************
// **** AUTOLOADING *****
// **********************
// Enable RobotLoader - this will load all classes automatically
$configurator->setTempDirectory(__DIR__ . '/../temp');
$configurator->createRobotLoader()
    ->addDirectory(__DIR__)
    ->addDirectory(__DIR__ . '/../libs')
    ->register();

// *******************************
// **** DEPENDENCY INJECTION *****
// *******************************
// Create Dependency Injection container from config.neon file
$configurator->addConfig(__DIR__ . '/config/config.neon', Nette\Configurator::AUTO);
$container = $configurator->createContainer();

return $container;
