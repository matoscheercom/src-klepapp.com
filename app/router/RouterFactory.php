<?php

use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;

/**
 * Router factory.
 */
class RouterFactory extends Nette\Object {

    /**
     * @return Nette\Application\IRouter
     */
    public function createRouter() {

        $router = new RouteList();

        $router[] = new Route('/', 'Jobs:redir', Route::ONE_WAY);
        $router[] = new Route('/jobs', 'Jobs:default');
        return $router;
    }
}
