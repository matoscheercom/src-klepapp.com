<?php

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Mail\Message;

/**
 * Contact Form component
 * @author Michal Bystricky <michal@fatchilli.com>
 */
class ContactForm2 extends Control {

    /** @var Mailer */
    private $mailer;

    private $selectOptions = [
        0 => 'Vyber si pozíciu',
        1 => 'Server Cloud Developer',
        2 => 'Android Developer',
        3 => 'iOS Developer'
    ];


    /**
     * Mailer injector
     * @param Mailer $mailer
     */
    public function injectMailer(Mailer $mailer) {
        $this->mailer = $mailer;
    }

    /**
     * @return Form
     */
    public function createComponentContactForm() {
        $f = new Form();

        $tm = $this->presenter->context->getParameters();

        $f->addSubmit('submit', "Bonzni ho za odmenu");
        $f->addText('name', "Meno kamaráta")
            ->setAttribute('placeholder', "Meno kamaráta*")
            ->setRequired('Nezadali ste meno.');

        $f->addText('email', "Tvoj e-mail*")
            ->setAttribute('placeholder', "Tvoj e-mail*")
            ->addRule(Form::EMAIL, 'Nesprávne zadaná e-mailová adresa.')
            ->setRequired('Nesprávne zadaná e-mailová adresa.');

        $f->addText('email2', "E-mail kamaráta*")
            ->setAttribute('placeholder', "E-mail kamaráta*")
            ->addRule(Form::EMAIL, 'Nesprávne zadaná e-mailová adresa.')
            ->setRequired('Nesprávne zadaná e-mailová adresa kamaráta.');

        $f->addText('phone', "Telefón kamaráta")
            ->setAttribute('placeholder', "Telefón kamaráta")
            ->setRequired('Nezadali ste telefón.');

        $f->addSelect('jobs','vyberte si job',$this->selectOptions)
            ->setAttribute('placeholder','Viber si poziciu')
            ->addRule(Form::MIN, "Nevybrali ste pozíciu.", 1);

        $f->addText('linkedin', "LinkedIn profil kamaráta")
            ->setAttribute('placeholder', "LinkedIn profil kamaráta");

        $f->addText("msg", "Message");

        $f->onSuccess[] = $this->contactFormSubmitted;
        $f->onError[] = $this->refresh;

        return $f;
    }

    public function refresh() {
        $this->redrawControl();
    }

    /**
     * @param \Nette\Application\UI\Form $f
     */
    public function contactFormSubmitted(Form $f) {
        $v = $f->getValues();

        if (!trim($v->msg) && !stristr($v->linkedin, 'url=')) {

            $params = $this->presenter->context->getParameters();
            $emailAddress = $params['mail']['contactForm'];

            $body = "Na stránke Klepapp.com bol vyplnený kontaktný formulár - Som bonzák.\n\n"
                    . "Pozicia: " . $this->selectOptions[$v->jobs] . "\n"
                    . "Bonzákov E-mail: " . $v->email . "\n\n"
                    . "Meno kamaráta: " . $v->name . "\n"
                    . "E-mail kamaráta: " . $v->email2 . "\n"
                    . "Telefón kamaráta: " . $v->phone . "\n"
                    . "LinkedIn kamaráta: \n\n" . $v->linkedin;

            $message = new Message;
            $message
                ->setSubject("Kontaktný formulár")
                ->addTo($emailAddress)
                ->setBody($body);

            $this->mailer->sendMessage($message);
        }

        $this->template->emailSent = true;

        $this->redrawControl();
        $this->presenter->redrawControl();
    }


    public function render() {
        $tm = $this->presenter->context->getParameters();
        $this->template->contact = $tm['contact'][$this->presenter->lang];
        $this->template->lang = $this->presenter->lang;

        $this->template->setFile(__DIR__ . '/contactForm2.latte');
        $this->template->render();
    }

}