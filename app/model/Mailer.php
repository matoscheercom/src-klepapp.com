<?php

use Nette\Mail\IMailer;
use Nette\Object;
use Nette\Mail\Message;

class Mailer extends Object {

    /** @var IMailer */
    private $mailer;

    /** @var array */
    private $parameters;

    public function __construct(IMailer $mailer, array $parameters = array()) {
        $this->parameters = $parameters;
        $this->mailer = $mailer;
    }

    public function sendMessage(Message $message) {
        $message->setFrom(
            $this->parameters['email'],
            $this->parameters['name']
        );

        $this->mailer->send($message);
    }
}