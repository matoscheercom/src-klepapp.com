<?php

/**
 * Base presenter
 * @author Michal Bystricky <michal@fatchilli.com>
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter {

    /** @var Mailer */
    protected $mailer;

    /** @persistent */
    public $lang = 'sk';

    /**
     * @param Mailer $mailer
     */
    public function injectMailer(Mailer $mailer) {
        $this->mailer = $mailer;
    }



    /**
     * @return ContactForm
     */
    public function createComponentContactForm1() {
        $c = new ContactForm1();
        $c->injectMailer($this->mailer);
        return $c;
    }

    /**
     * @return ContactForm
     */
    public function createComponentContactForm2() {
        $c = new ContactForm2();
        $c->injectMailer($this->mailer);
        return $c;
    }


    public function beforeRender() {
        parent::beforeRender();

        switch($this->lang) {
            case 'sk':
//            case 'cs':
//            case 'en':
//            case 'de':
                $this->template->lang = $this->lang;
                break;
            default:
                $this->redirect('this', array('lang' => 'sk'));
                $this->terminate();
        }
        
        $tm = $this->context->getParameters();
        $this->template->head = $tm['head'][$this->lang];
        $this->template->wanted = $tm['wanted'][$this->lang];
        $this->template->testimonials = $tm['testimonials'][$this->lang];
        $this->template->lookfor = $tm['lookfor'][$this->lang];
        $this->template->contact = $tm['contact'][$this->lang];
        $this->template->navigation = $tm['navigation'][$this->lang];
    }
}
