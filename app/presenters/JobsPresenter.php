<?php

/**
 * Homepage presenter
 * @author Martin Scheer <martin.scheer@fatchilli.com>
 */
class JobsPresenter extends BasePresenter {

    public function actionRedir() {
        $this->redirect(302, "default");
    }

}