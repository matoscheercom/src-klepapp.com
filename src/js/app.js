(function() {
    App = function() {

        var scroolToIdElement = function() {
            var isAnimation = false;
            jQuery('.js-scroll').on('click', function(e) {
                e.preventDefault();
                if (isAnimation === false) {
                    isAnimation = true;
                    var linkId = $(this).attr('href');
                    var elmentOffset = $(linkId).offset().top;
                    jQuery('html, body').animate({scrollTop: elmentOffset }, 600);
                    setTimeout(function() {isAnimation = false}, 600);
                }
            });
        };

        var fancyBoxHendler = function() {
            $(".various").fancybox({
                maxWidth	: 800,
                maxHeight	: 600,
                fitToView	: false,
                width		: '70%',
                height		: '70%',
                autoSize	: false,
                closeClick	: false,
                openEffect	: 'none',
                closeEffect	: 'none'
            });
        }

        return {
            init: function() {
                jQuery(document).ready(function() {
                    scroolToIdElement();
                    fancyBoxHendler();
                });
            }
        }
    }();

    App.init();

})();