# Require any additional compass plugins here.
require 'susy'

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "www/css"
sass_dir = "src/scss"
images_dir = images_path = "www/images"
#fonts_dir = fonts_path = "www/fonts"
#javascripts_dir = "www/js"
# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

#http_fonts_path = http_path + "fonts/"
http_generated_images_path = http_images_path = http_path + "images/"

line_comments = false

#output_style = :compressed

asset_cache_buster :none
